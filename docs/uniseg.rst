=============
:mod:`uniseg`
=============

.. automodule:: uniseg
   :members:

.. autodata:: uniseg.__version__
