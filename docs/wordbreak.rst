======================================
:mod:`uniseg.wordbreak` --- Word Break
======================================

.. automodule:: uniseg.wordbreak
   :members:
