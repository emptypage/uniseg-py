====================================
:mod:`uniseg.wrap` --- Text Wrapping
====================================

.. automodule:: uniseg.wrap
   :members:
