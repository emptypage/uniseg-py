=======
License
=======

.. literalinclude:: ../LICENSE
    :language: none

(This is the output of the sample script, ``uniwrap.py`` with the option
``-w 76``.)
