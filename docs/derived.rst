=================================================
:mod:`uniseg.derived` --- Derived Core Properties
=================================================

.. automodule:: uniseg.derived
   :members:
