======
uniseg
======


A Python package to determine Unicode text segmentations.

Table of Contents
=================

.. toctree::
   :maxdepth: 2
   :numbered:

   introduction
   unicodeversions
   modules
   samples
   license

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
