==============================================
:mod:`uniseg.sentencebreak` --- Sentence Break
==============================================

.. automodule:: uniseg.sentencebreak
   :members:
