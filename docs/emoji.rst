========================================
:mod:`uniseg.emoji` --- Emoji Properties
========================================

.. automodule:: uniseg.emoji
   :members:
